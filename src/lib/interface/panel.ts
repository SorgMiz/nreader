export interface IPanel {
    title: string;
	content: string;
	recieveMessage: (message: any) => any;

	on(eventName: string, callback: (e: any) => any): void;
	off(eventName: string): void;
	update(): void;
	show(): void;
}
