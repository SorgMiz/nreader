import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

import * as nreader from '../nreader';
import { IPanel } from '../interface/panel';

/**
 * WebViewパネルのラッパー
 */
export class BasePanel implements IPanel {
	private _title: string;
	private _content: string;
	private _recieveMessage: (e: any) => any;	// 作成時に設定するのみ
	private _webViewPanel: vscode.WebviewPanel | null;
	private _event: {[key: string]: (e: any) => any };

	/**
	 * コンストラクタ
	 */
	public constructor() {
		this._webViewPanel = null;

		this._title = '';
		this._content = '';
		this._recieveMessage = (message: any) => {
			vscode.window.showInformationMessage('recieveMessageCallbackが設定されていない');
		};

		this._event = {
			'dispose': (e) => {},
			'create': (e) => {},
		};
	}

	public get title(): string {
		return this._title;
	}

	public set title(title: string) {
		this._title = title;
	}

	public get content(): string {
		return this._content;
	}

	public set content(content: string) {
		this._content = content;
	}

	public get recieveMessage(): (message: any) => any {
		return this._recieveMessage;
	}

	public set recieveMessage(callback: (message: any) => any) {
		this._recieveMessage = callback;
	}

	public on(eventName: string, callback: (e: any) => any) {
		if (eventName in this._event) {
			this._event[eventName] = callback;
		}
	}

	public off(eventName: string) {
		if (eventName in this._event) {
			this._event[eventName] = (e) => {};
		}
	}

	protected getWebViewPanel(): vscode.WebviewPanel {
		if (this._webViewPanel === null) {
			throw new Error('パネルが作成されていません');
		}

		return this._webViewPanel;
	}

	protected set webViewPanel(panel: vscode.WebviewPanel | null) {
		this._webViewPanel = panel;
	}

	protected get webViewPanel(): vscode.WebviewPanel | null {
		return this._webViewPanel;
	}

	public update() {
		try {
			if (this._webViewPanel === null) {
				throw new Error('WebViewパネルが存在していません');
			}
	
			this._webViewPanel.title = this.title;
			this._setHtml(this.content);


		} catch(e) {
			this._webViewPanel = null;
			
		}

	}

	/**
	 * WebViewを表示する。なければ作成する
	 */
	public show(): void {
		try {
			// WebViewのパネルがない、閉じられているならエラーを発生させる
			this.getWebViewPanel().title = '';


		} catch(e) {
			this._webViewPanel = null;
			this._createWebView();
		}

		this.update();

	}
		
	/**
	 * WebViewパネルの作成
	 */
	private _createWebView() : void {

		// ・retainContextWhenHiddenでタブ内容がバックグラウンドになっても維持する
		this.webViewPanel = vscode.window.createWebviewPanel(
			'',
			this.title,
			vscode.ViewColumn.Active,
			{
				retainContextWhenHidden: true,
				enableScripts: true
			}
		);
	
		this.webViewPanel.webview.onDidReceiveMessage(this.recieveMessage, undefined, nreader.getContext().subscriptions);
	
		this.getWebViewPanel().onDidDispose((e) => {
			// WebViewを放棄
			this._webViewPanel = null;

			// イベントを発生させる
			this._event.dispose(e);
		});

		this._event.create(this);
	}

	/**
	 * WebViewに受け取ったHTML本体部分をセットする
	 */
	private _setHtml(body: string) {
    	/// cspのnonceで指定したBASE64のコードを持つSTYLEが有効になる
    	const nonce = BasePanel._getNonce();
    	const csp = `default-src 'none'; style-src 'nonce-${nonce}'; script-src 'unsafe-inline';`;
		const js = fs.readFileSync(path.join(nreader.getDir('js'), 'script.js'), 'utf8');
		const colorConfig = nreader.getConfig('StateColor');

		let style = fs.readFileSync(path.join(nreader.getDir('css'), 'style.css'), 'utf8');

		style += `span.NewPage {color: ${colorConfig.New.StateColor};}`;
		style += `span.ModPage {color: ${colorConfig.Mod.StateColor};}`;
		style += `span.UnreadPage {color: ${colorConfig.Unread.StateColor};}`;
		style += `span.ReadPage {color: ${colorConfig.Read.StateColor};}`;

		if (this.webViewPanel === null) {
			throw new Error('WebVieｗPanelが作成されていません');
		}

		// HTMLのヘッダー部と合わせてセットする
		this.webViewPanel.webview.html = `
			<!DOCTYPE htt" content="width=device-ml>
			<html lang="ja">
			<head>
			<meta charset="UTF-8">
			<meta http-equiv="Content-Security-Policy" content="${csp}">
			<meta name="viewporwidth, initial-scale=1.0">
			
			<style type="text/css" nonce="${nonce}">
			${style}
			</style>
			<script type="text/javascript">
			${js}
			</script>
			</head>
			<body>
			${body}
			</body>
			</html>
		`;
	}

	/**
	 * CSPで使用するNonceを取得
	 */
	private static _getNonce(): string {
		let text = '';
		const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		for (let i = 0; i < 32; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
}
