import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

import * as nreader from '../nreader';
import * as treeItem from '../treeItem';
import * as type from '../type/pageInfo';
import * as novels from '../novels';
import { NovelProvider } from './novelProvider';

/**
 * 本棚ビューのTreeViewProvider
 */
export class BookShelfProvider extends NovelProvider {
    protected _stateFile = 'bookshelf.json';

    private _updating = false;

    constructor() {
        super();
        this._state.ncodeList = [];
    }
    
    /**
     * TreeViewProviderのメソッド
     * Providerが管理しているNコードをNovelItemのリストで返す
     * @param {*} treeItem 親要素。ルートの場合はundefined
     */
    public getChildren(novelItem: treeItem.NovelItem): treeItem.NovelItem[] {
        let lists: treeItem.NovelItem[] = [];

        for(let i in this._state.ncodeList) {
            lists.push(novels.get(this._state.ncodeList[i]));
        }

        return lists;
    }

    /**
     * 新しく追加
     * @param {*} novelItem
     */
    public addNovel(novelItem: treeItem.NovelItem): boolean {
        const novelInfo = novelItem.getInfo();
        const ncodePath = path.join(nreader.getDataPath(), novelInfo.code);

        // ページデータを保存するディレクトリがなければ作成する
        if (!fs.existsSync(ncodePath)) {
            fs.mkdir(ncodePath, { recursive: true }, (err: any) => {
                vscode.window.showErrorMessage(err);
                return false;
            });
        }

        this._state.ncodeList.push(novelInfo.code);

        return true;
    }

    /**
     * 削除
     * @param {*} novelItem
     */
    public removeNovel(novelItem: treeItem.NovelItem): boolean {
        let novelInfo = novelItem.getInfo();

        // Nｺｰﾄﾞのディレクトリ
        const ncodePath = path.join(nreader.getDataPath(), novelInfo.code);

        // ディレクト単位で削除
        if (! fs.existsSync(ncodePath)) {
			vscode.window.showWarningMessage('既に削除されています。');
			return false;
		}

		let files = fs.readdirSync(ncodePath);
		for (let file of files) {
			fs.unlinkSync(path.join(ncodePath, file));
		}
		fs.rmdirSync(ncodePath);

		novelItem.clearPage();

		novelInfo.update = "";

		// 管理リストから削除
		this._state.ncodeList.splice(this._state.ncodeList.indexOf(novelInfo.code), 1);
		this.state.save();

		return true;
    }

    /**
     * 登録されたNovelItemの更新チェックを非同期で行う
     */
    public async checkUpdate(): Promise<any> {

        if (this._updating) {
            vscode.window.showInformationMessage('更新チェック中です');
            return;
        }

        this._updating = true;

        for (let ncode of this._state.ncodeList) {
            await novels.get(ncode).checkUpdate();
        }

        this._updating = false;
        
        // 更新情報を保存
        novels.save();
    }
}