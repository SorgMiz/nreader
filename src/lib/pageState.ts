export enum PageState {
	New = 'New',
	Unread = 'Unread',
	Read = 'Read',
	Mod = 'Mod'
}
