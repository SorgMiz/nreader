
import * as vscode from 'vscode';
import * as nreader from './lib/nreader';

function activate(context: vscode.ExtensionContext) {
    try {
        nreader.init(context);

    } catch (error) {
        vscode.window.showErrorMessage(error);
        console.log(error.stack);
    }
}

exports.activate = activate;
exports.deactivate = function () {};

