import * as vscode from 'vscode';

import * as nreader from '../nreader';
import * as novels from '../novels';
import * as treeItem from '../treeItem';

import { ItemPanel } from './itemPanel';

export class PagePanel extends ItemPanel {

    public constructor(pageItem: treeItem.PageItem) {
        super(pageItem);
    }

    /**
     * 保持しているPageItemの親であるNovelItemを取得
     */
    protected getNovelItem(): treeItem.NovelItem {
        return novels.get(this.pageItem.getInfo().code);
    }

    protected get pageItem(): treeItem.PageItem {
        return this.item as treeItem.PageItem;
    }

    protected set pageItem(pageItem: treeItem.PageItem) {
        this.item = pageItem;
    }

    /**
     * ページヘッダー
     */
    protected _pageHeader(): string {
        let info = this.pageItem.getInfo();

        return '<h3>' + this.getNovelItem().getInfo().title + "</h3>\n"
            + '<h1>' + info.title + "</h1>\n"
            + '<h5><span class=lastUpdate>最終更新日：' + info.update + "</span></h5>\n";
    }

    /**
     * コンテンツをフィルタリングする
     * ・カッコを強調
     * @param content コンテンツ
     */
    protected _contentFilter(content: string): string {
        content = content.replace(/(「|『)/g, '<em>$1');
        content = content.replace(/(」|』)/g, '$1</em>');

        return content;
    }

    /**
     * ページフッター部
     * ページ移動コントロールのHTMLを返す
     */
    protected _pageFooter(): string {
        const info = this.pageItem.getInfo();

        // ページの移動先
        // urlが空文字のPageItemはチャプター
        let list = this.getNovelItem().getPageItemList();
        let move = { prev: -1, next: -1 };
        let index: number = -1;

        for (let i = 0; i < list.length; i++) {

            // 自インデックス前
            if (index === -1) {
                if (i > 0 && list[i - 1].getInfo().url !== '') {
                    move.prev = i - 1;
                }
            }

            if (info.url === list[i].getInfo().url) {
                index = i;
            }

            if (index !== -1) {
                // 自インデックスが見つかった後、次の有効なページデータがあれば、それを次ページのインデックスにする
                if (i + 1 < list.length && list[i + 1].getInfo().url !== '') {
                    move.next = i + 1;
                    break;
                }
            }
        }

        // ページ移動リンク
        let prev, next;
        let item: treeItem.PageItem;
        let state: string;

        if (move.prev === -1) {
            prev = `<span class=disable>前ページなし &lt;&lt; </span>`;
        } else {
            item = list[move.prev];
            state = item.getPageState();

            prev = `<span onClick="postMessage({ command: 'movePage', no: ${move.prev} } );return false;" class="enable ${state}Page">` + 
                    item.getInfo().title + ' &lt;&lt; </span>';
        }

        if (move.next === -1) {
            next = `<span class=disable> &gt;&gt; 次ページなし</span>`;
        } else {
            item = list[move.next];
            state = item.getPageState();

            next = `<span onClick="postMessage({ command: 'movePage', no: ${move.next} } );return false;" class="enable ${state}Page">` + 
                    ' &gt;&gt; ' + item.getInfo().title + '</span>';
        }

        return `${prev}　${next}`;
    }

    /**
     * WebViewを表示。表示後、Resolveする
     * ※ ページをサーバーから取得してから表示させる
     */
    public show(): Promise<any> {
        const pageItem = this.pageItem;

        pageItem.loading = true;

		return new Promise((resolve, reject) => {
			this.updateContent()
            .then(() => {

                // TreeViewを更新
                nreader.refreshProviders();

                // ページ一覧を更新
                this.getNovelItem().updatePageList();

				super.show();
				
				resolve();

            })
            .catch(reason => {
				vscode.window.showErrorMessage(reason);
				
				reject();

            })
            .finally(function () {
                setTimeout(() => {
                    pageItem.loading = false;
                }, 250);
            });
		});
    }

    /**
     * PostMessagのコールバック
     * @param message 
     */
    protected recieveMessageCallback(message: any): void {

        // ページ移動リンク
        switch (message.command) {
            case 'movePage':
       
                // このパネルのPageItemを差し替えて、表示させる
                const pageItem = this.getNovelItem().getPageItemList()[parseInt(message.no)];

				// 移動先ページを表示
                const pagePanel = new PagePanel(pageItem);
				pagePanel.show()
				.then(() => {
					// 移動先ページの表示後、自ページのコンテンツを更新する
					return this.updateContent();
				})
				.then(() => {
					// 更新されたコンテンツで更新する
					this.update();
				});
				
                return;
        }
	}
	
	/**
	 * 保持コンテンツを更新
	 */
	private updateContent() {
		const pageItem = this.pageItem;
		
		return new Promise((resolve, reject) => {
			pageItem.getPageContent()
            .then(content => {
                let body: string = '';

                body += this._pageHeader();
                body += this._contentFilter(content);
                body += this._pageFooter();

                this.content = body;
				
				resolve();

            })
            .catch(reason => {
				reject(reason);

            });
		});
	}
}