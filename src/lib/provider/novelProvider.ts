import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

import * as nreader from '../nreader';
import * as treeItem from '../treeItem';
import * as novels from '../novels';
import { BaseProvider } from './baseProvider';

/**
 * 一階層目がNovelItemのTreeViewプロバイダ
 */
export abstract class NovelProvider extends BaseProvider {

    /**
     * TreeViewProviderのメソッド
     * Providerが管理しているNコードをNovelItemのリストで返す
     * @param {*} treeItem 親要素。ルートの場合はundefined
     */
    public getChildren(novelItem: treeItem.NovelItem): treeItem.NovelItem[] {
        let lists: treeItem.NovelItem[] = [];

        for(let ncode in this._state) {
            lists.push(novels.get(ncode));
        }

        return lists;
    }

    public abstract addNovel(novelItem: treeItem.NovelItem): boolean;
    public abstract removeNovel(novelItem: treeItem.NovelItem): boolean;
}
