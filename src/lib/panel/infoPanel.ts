import * as treeItem from '../treeItem';
import { ItemPanel } from './itemPanel';

/**
 * 情報パネル
 */
export class InfoPanel extends ItemPanel {

	public constructor(novelItem: treeItem.NovelItem) {
		super(novelItem);
	}

	protected recieveMessageCallback(message: any): void {
		throw new Error("Method not implemented.");
	}
	
	protected get novelItem(): treeItem.NovelItem {
		return this.item as treeItem.NovelItem;
	}

	public get title() {
		const novelInfo = this.novelItem.getInfo();
		return novelInfo.title;
	}

	public get content(): string {
		const novelInfo = this.novelItem.getInfo();
	
		let ex = novelInfo.ex.replace(/\n/g, '<br>');
	
		return `
			<h2 class=title>${novelInfo.title}</h2>\n
			<span class=auth>作者：${novelInfo.author}</span>
			<span class=ncode>Ｎコード：${novelInfo.code}</span>
			<span class=lastUpdate>最終更新日：${novelInfo.lastUpdate}</span>
			<br><br>${ex}
		`;
	}
}
