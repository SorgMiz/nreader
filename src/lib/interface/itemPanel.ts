import { IPanel } from "./panel";
import { INrItem } from "./nrItem";

export interface IItemPanel extends IPanel {
    updateItem(item: INrItem): void;
}
