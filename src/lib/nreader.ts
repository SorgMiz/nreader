import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as axios from 'axios';
import * as cheerio from 'cheerio';

import * as provider from './provider';
import * as treeItem from './treeItem';
import * as type from './type';
import * as novels from './novels';
import { NovelItem, PageItem } from './treeItem';

// 小説情報取得URL
const NOVEL_DATA_URL = "https://ncode.syosetu.com/novelview/infotop/ncode/{ncode}/";

const CONFIG_FILE = 'config.json';

let _config: { [index: string] : any};
let _context: vscode.ExtensionContext;
let _provider: { [index: string]: provider.NovelProvider } = {
    bookShelf: new provider.BookShelfProvider()
};

/**
 * 初期化
 */
export function init(context: vscode.ExtensionContext): void {
    _context = context;

	const configFile = path.join(getDir('json'), CONFIG_FILE);

	if (!fs.existsSync(configFile)) {
		fs.mkdirSync(getDir('json'), { recursive: true });
		fs.copyFileSync(path.join(getDir('base'), CONFIG_FILE), configFile);
	}

	_config = JSON.parse(fs.readFileSync(configFile, 'utf8'));

	// configの色設定に合わせてSVGを更新する
	updateResource();

    // dataディレクトがなければ作成
//    if (!fs.existsSync(getDir('data'))) {
//        fs.mkdir(getDir('data'), { recursive: true }, (err?) => {
    if (fs.existsSync(getDataPath())) {
        main();

    } else {

        fs.mkdir(getDataPath(), { recursive: true }, (err?) => {
            if (err === null) {
                main();
            } else {
                console.log(err.stack);
                throw new Error(err.toString());
            }
        });
	}
	
}

export function main() {
    // 小説情報のキャッシュの読み込み
    novels.load();

    // ビュー作成
    //vscode.window.createTreeView('searchView', { treeDataProvider: _provider.searchNovel });
    vscode.window.createTreeView('bookShelfView', { treeDataProvider: _provider.bookShelf });

    // 各プロバイダで前回の状態をロード、表示を更新させる
    //_provider.searchNovel.state.load();
    _provider.bookShelf.state.load();

    /**
     * コマンド登録
     * ※パレットには登録しない
     */

     // 本棚更新
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.updateBookShelf', () => {
        (_provider.bookShelf as provider.BookShelfProvider).checkUpdate();
    }));
    
    // 小説検索
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.searchNovel', () => {
        command.searchNovel();
    }));
    
    // 小説更新
	 _context.subscriptions.push(vscode.commands.registerCommand('novelReader.updateNovel', (novelItem: NovelItem) => {
        novelItem.checkUpdate();
	}));
	
    // 小説削除
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.removeNovel', (novelItem: NovelItem) => {
       command.removeNovel(novelItem);
    }));
    
    // 小説選択(本棚の小説がクリックされた時に呼び出される)
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.selectNovel', (novelItem: NovelItem) => {
        command.selectNovel(novelItem);
    }));

    // ページ選択
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.selectNovelPage', (pageItem: PageItem) => {
        command.selectNovelPage(pageItem);
    }));
    
    // 小説情報の表示
    _context.subscriptions.push(vscode.commands.registerCommand('novelReader.novelInfo', (novelItem: NovelItem) => {
        command.showNovelInfo(novelItem);
    }));
}

/**
 * データのパス取得
 * ※ 拡張機能の直下だと更新時に削除される為
 */
export function getDataPath() {
    return path.join(_context.globalStoragePath, 'data');
}

/**
 * この拡張機能のパスを基準に、指定のサブディレクトリを返す
 * @param {String} dirName 
 */
export function getDir(dirName: string | undefined): string {
	return dirName === undefined ? _context.extensionPath : path.join(_context.extensionPath, dirName);
}

/**
 * SearchNovelProviderを返す
 */
//export function remove_getSearchProvider(): provider.SearchNovelProvider {
//    return _provider.searchNovel as provider.SearchNovelProvider;
//}

/**
 * BookShelfProviderを返す
 */
export function getBookShelfProvider(): provider.BookShelfProvider {
    return _provider.bookShelf as provider.BookShelfProvider;
}

/**
 * ExtensionContextを返す
 */
export function getContext():vscode.ExtensionContext {
    return _context;
}

export function getConfig(key: string | null = null): { [index: string]: any;} {
    if (key === null) {
        return _config;
    }
	return _config[key];
}

export function getProviders(): { [index: string]: provider.NovelProvider } {
    return _provider;
}

/**
 * Providerを更新する
 */
export function refreshProviders(): void {
    // 各Providerを通して、ビューを更新させる
    for (const providerName in _provider) {
        _provider[providerName].refresh();
    }
}

/**
 * コマンドの処理
 */
export namespace command {

    /**
     * 検索
     */
    export async function searchNovel(): Promise<any> {
        let word: any = await vscode.window.showInputBox({ placeHolder: '検索ワードを入力' });

        if (word) {
            novels.search(word);
        }
    }

    /**
     * 本棚追加
     * @param {*} novelItem 
     */
    export function addNovel(ncode: string): boolean {
        
        if (novels.exists(ncode)) {
            vscode.window.showInformationMessage('既に登録されています');
            return false;
        }

        // Nコードで小説情報を取得
        getNovelInfo(ncode).then(novelInfo => {
            if (novelInfo === undefined) {
                showError(`指定の小説情報が取得出来ませんでした。(Nコード:${ncode})`);
                return;
            }

            let novelItem = new treeItem.NovelItem(novelInfo);
            
            novels.add(novelItem);
            novels.save();
        
            getBookShelfProvider().addNovel(novelItem);
            
            refreshProviders();
        });
        
        return true;
    }

    /**
     * 削除
     * @param {*} novelItem 
     */
    export function removeNovel(novelItem: treeItem.NovelItem): void {

		let items = [{label: 'OK'}, {label: 'CANCEL'}];

        var options: vscode.QuickPickOptions = {
            matchOnDescription: true,
            placeHolder: "削除しますか？"
        };
        
        vscode.window.showQuickPick(items, options)
        .then(item => {
            if (item !== undefined && item.label === 'OK') {
		
				let novelInfo = novelItem.getInfo();
				
				// 管理課のProviderに削除させる
				for(let provider in _provider) {
					_provider[provider].removeNovel(novelItem);
				}
		
				novels.remove(novelInfo.code);
				novels.save();
				
				refreshProviders();
            }
		});
		
    }

    /**
     * ページ選択
     * @param {*} NovelItem 
     */
    export function selectNovel(NovelItem: treeItem.NovelItem): void {

        if (NovelItem.getInfo().url === '') {
            return;
        }
        
        NovelItem.selectNovel();
    }
    
    /**
     * ページ選択
     * @param {*} pageItem 
     */
    export function selectNovelPage(pageItem: treeItem.PageItem): void {

        if (pageItem.getInfo().url === '') {
            return;
        }
        pageItem.selectPage();
    }
    
    /**
     * 小説情報の表示
     * @param novelItem 
     */
    export function showNovelInfo(novelItem: treeItem.NovelItem): void {
		novelItem.showInfoPage();
    }
}

export function showError(message: string): void {
    vscode.window.showWarningMessage(message);
    throw new Error(message);
}

/**
 * Nコードから小説情報を取得
 */
export async function getNovelInfo(ncode: string) : Promise<type.NovelInfo | undefined> {

    // 情報取得
    let url = NOVEL_DATA_URL.replace(/{ncode}/, ncode);
    let ua = getConfig('UserAgent');
    
    let response: axios.AxiosResponse;

    try {
        response = await axios.default({ method: 'GET', url: url, responseType: 'document', headers: {"User-Agent": ua} });
    
        if (response.status !== 200) {
            return;
        }
    } catch(err) {
        vscode.window.showInformationMessage(`Nコード：${ncode}の小説情報の取得に失敗しました。(${err})`);
        return;
    }
    
    const $ = cheerio.load(response.data,  { decodeEntities: false });
    const titleElem = $('h1', 'div#contents_main');		// タイトルを含むエレメント
    const novelTableElement = $('table#noveltable1');	// 小説情報を含むテーブルのエレメント
    const novelUpdateElement = $('table#noveltable2');	// 更新日を含むエレメント

    let lastUpdateMatches = $(novelUpdateElement).text().match(/.+(?:更新|掲載)日\n+(?:(\d{4})年\s(\d{2})月(\d{2})日\s(\d{2})時(\d{2}))/);
    let lastUpdate = '';

    if (lastUpdateMatches === null) {
        vscode.window.showInformationMessage('ページがありません');

        return;
    } else {
        lastUpdate = lastUpdateMatches[1] + '/' + lastUpdateMatches[2] + '/' + lastUpdateMatches[3] + ' ' + lastUpdateMatches[4] + ':' + lastUpdateMatches[5];
    }
    
    return {	
        title: titleElem.text(),
        url: $('a', titleElem).attr('href')!,
        code: ncode,
        author: $('th:contains("作者名")', novelTableElement).next().text(),
        ex: $('td.ex', novelTableElement).text(),
        lastUpdate: lastUpdate,
        update: null
    };
}

/**
 * アイコンの作成
 * json/config.jsonの変更があれば呼び出す
 */
export function updateResource() {

	// SVGアイコンの更新

	// ページの状態をファイル名で表すための配列
	const stateList: {[key: string]: string} = {
		'Read': '',
		'Unread': '_page_unread',
		'Mod': '_page_mod',
		'New': '_page_new'
	};

	type updateSetting = {
		Prefix: string,
		Template: string
	};

	const update: {[key: string]: updateSetting, Update: updateSetting} = {
		'None': {
			'Prefix': '', 
			'Template': fs.readFileSync(path.join(getDir('base'), 'novel_base.svg')).toString()
		},
		'Update': {
			'Prefix': '_update', 
			'Template': fs.readFileSync(path.join(getDir('base'), 'novel_update_base.svg')).toString()
		}
	};

	for(let us in update) {
		for(let state in stateList) {
			let svg = path.join(getDir('images'), 'novel' + update[us].Prefix + stateList[state] + '.svg');
	
			let buf = update[us].Template;

			buf = buf.replace('${StateColor}', _config.StateColor[state].StateColor);
			buf = buf.replace('${UpdateColor}', _config.StateColor[state].UpdateColor);

			fs.writeFileSync(svg, buf);
		}
	}
}