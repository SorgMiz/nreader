const vscode = acquireVsCodeApi();

function postMessage(command) {
	vscode.postMessage(command);
}