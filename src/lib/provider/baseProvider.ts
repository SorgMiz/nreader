import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

import * as nreader from '../nreader';

/**
 * TreeViewProviderの共通クラス
 */
export abstract class BaseProvider {
    protected _stateFile: string = 'state.json';	// 状態ファイル
    protected _state: {[key: string]: any} = {};	// 保持しているNコードのリスト
    protected _onDidChangeTreeData = new vscode.EventEmitter();


    public onDidChangeTreeData = this._onDidChangeTreeData.event;
    /**
     * TreeViewProviderのメソッド
     * TreeItemを取得
     * ※ 子要素がTreeItem以外のオブジェクトで管理している場合、引数にそれが渡ってくるので、ここでTreeItemで返ようにする
     * @param {*} treeItem
     */
    public getTreeItem(treeItem: vscode.TreeItem): vscode.TreeItem {
        return treeItem;
    }

    /**
     * TreeViewProviderのメソッド
     * Providerが管理しているNコードをNovelItemのリストで返す
     * @param {*} treeItem 親要素。ルートの場合はundefined
     */
    public abstract getChildren(treeItem: vscode.TreeItem): vscode.TreeItem[];

    /**
     * 状態の取得・保存を行うオブジェクトを返す
     */
    get state(): {save: () => void, load: () => void} {
        const self = this;

        return {
            save: function () {
                self._saveState();
            },
            load: function () {
                self._loadState();
            }
        };
    }

    /**
     * 状態ファイルの取得
     */
    protected _getStateFile(): string {
        return path.join(nreader.getDataPath(), this._stateFile);
    }

    /**
     * 状態を保存
     */
    protected _saveState(): void {
        fs.writeFileSync(
            this._getStateFile(),
            JSON.stringify(this._state)
        );
    }

    /**
     * 状態の読み込み
     */
    protected _loadState(): void {

        if (!fs.existsSync(this._getStateFile())) {
            this.state.save();
            return;
        }

        // 情報を書き込み
        let json = JSON.parse(fs.readFileSync(this._getStateFile()).toString());

        this._state = json;
    }

    /**
     * 表示更新
     */
    public refresh(): void {
        this.state.save();
        this._onDidChangeTreeData.fire(null);
    }
    
}
