import { BaseInfo } from "./baseInfo";

/**
 * 小説情報
 */
export type NovelInfo = BaseInfo & {
    author: string;
    ex: string;
};
