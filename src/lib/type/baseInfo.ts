
/**
 * NovelItem, PageItem共通の情報保持
 */
export type BaseInfo = {
    title: string;
    url: string;
    code: string;
    lastUpdate: string;
    update: string | null;
};
