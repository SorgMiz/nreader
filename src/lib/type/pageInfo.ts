import { BaseInfo } from "./baseInfo";

/**
 * ページ情報
 */
export type PageInfo = BaseInfo & {
    no: number | null;
    cached: boolean;
};
