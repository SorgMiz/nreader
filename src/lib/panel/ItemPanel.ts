import * as treeItem from '../treeItem/baseItem';
import { BasePanel } from './basePanel';
import { IItemPanel } from '../interface/itemPanel';

/**
 * 小説、ページのTreeItemが保持している内容を出力する共通クラス
 */
export abstract class ItemPanel extends BasePanel implements IItemPanel {
	private _baseItem: treeItem.BaseItem;

	public constructor(baseItem: treeItem.BaseItem) {
		super();

		this._baseItem = baseItem;
		
		this.recieveMessage = (message) => {
			this.recieveMessageCallback(message);
		};
	}
	updateItem(item: any): void {
		throw new Error("Method not implemented.");
	}

	/**
	 * ページからのPostMessageを受け取るコールバック関数
	 */
	protected abstract recieveMessageCallback(message: any) : void;
	
	/**
	 * 保持しているTreeItemへアクセスするプロパティ
	 */
	protected get item(): treeItem.BaseItem {
		return this._baseItem;
	}
	protected set item(baseItem: treeItem.BaseItem) {
		this._baseItem = baseItem;
	}
	
	/**
	 * タイトル
	 */
	public get title() {
		return this.item.getInfo().title;
	}

	public set title(title: string) {
		throw new Error('このオブジェクトはタイトルは設定出来ません');
	}
}