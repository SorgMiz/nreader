import * as fs from 'fs';
import * as path from 'path';

import * as nreader from './nreader';
import * as treeItem from './treeItem';
import * as searchNovel from './searchNovel';
import * as type from './type';

const STATE_FILE = 'novel.json';
let novelList: { [index: string]: treeItem.NovelItem } = {};

/**
 * 指定Nコードの情報を取得
 */
export function get(ncode: string): treeItem.NovelItem {
    return novelList[ncode];
}

/**
 * NovelItemの追加
 */
export function add(novelItem: treeItem.NovelItem): void {
    var novelInfo: type.NovelInfo = novelItem.getInfo();
　
    // 初期値は更新フラグが立っている
    // 既存データと最終更新日が異なる場合、更新フラグを立てる
    if ( ! (novelInfo.code in novelList)) {
        novelList[novelInfo.code] = novelItem;
    }
}

export function remove(ncode: string): void {
    delete novelList[ncode];
}

export function save(): void {
    let writeData: { [index: string]: type.NovelInfo } = {};

    // 参照リストにないものは削除
    for (var key in novelList) {
        writeData[key] = novelList[key].getInfo();
    }

    fs.writeFileSync(
        path.join(nreader.getDataPath(), STATE_FILE),
        JSON.stringify({novels: writeData})
    );
}

export function load(): void {
    const cache = path.join(nreader.getDataPath(), STATE_FILE);

    if (!fs.existsSync(cache)) {
        fs.writeFileSync(cache, JSON.stringify( {novels: {}}));
    }

    // JSONを読み込み
    let buffer = fs.readFileSync(cache);
    let json = JSON.parse(buffer.toString());

    // 全クリア
    for (let key in novelList) {
        delete novelList[key];
    }

    for (let key in json.novels) {
        let novelItem = new treeItem.NovelItem(json.novels[key]);
        novelList[key] = novelItem;

        novelItem.getPageItemList().forEach((pageItem) => {
            if (pageItem.getInfo().update === null) {
                pageItem.getInfo().update = '';
            }
        });
            
        //Novels.novelList[key].getInfo().updateFlg = false;
    }
}

export function exists(ncode: string): boolean {
    return ncode in novelList;
}

export function search(word: string): void {
    searchNovel.search(word);
}
