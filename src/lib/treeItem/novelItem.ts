import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as axios from 'axios';
import * as cheerio from 'cheerio';

import * as nreader from '../nreader';
import * as novels from '../novels';
import * as panel from '../panel';
import * as type from "../type";
import { BaseItem } from './baseItem';
import { PageItem } from './pageItem';
import { INrItem } from '../interface/nrItem';

/**
 * 各Providerで使用するTreeItem
 * NReaderですべて管理される。
 */
export class NovelItem extends BaseItem implements INrItem {
    protected _info: type.NovelInfo;

	private _pageList: PageItem[];
	private _pageListPanel: panel.PageListPanel | null;

    public loading: boolean;

    constructor(info: type.NovelInfo) {
        //super(info, vscode.TreeItemCollapsibleState.Collapsed);
        super(info, vscode.TreeItemCollapsibleState.None);

        this._info = info;
		this._pageList = [];
		this._pageListPanel = null;

        // package.jsonの "when"キーで 「viewItem」として参照できる
        this.contextValue = 'Novel';

        this.loading = false;

        // TreeItemクリックされた際のコマンド
        this.command = {
            command: "novelReader.selectNovel",
            title: "Select Node",
            arguments: [this]
        };
        
        this.importPageState();
    }

    /**
     * 表示させるアイコンのパスを返す
     * 
     * ３つの状態がある
     * ページ全てが読み込まれている：白アイコン
     * 更新あり：赤アイコン
     * 未取得のページあり(新規、既読)：青アイコン 
     * 
     */
    public get iconPath(): string {
        const novelInfo: type.NovelInfo = this.getInfo();

        // ロード、更新中
        if (this.loading) {
            return path.join(nreader.getDir('images'), 'oval.svg');
        }

        //return path.join(nreader.getDir('images'), 'novel.svg');

        let iconFile = 'novel' + this._checkStatus();

        return path.join(nreader.getDir('images'), iconFile + '.svg');
    }
    
    /**
     * 
     */
    protected isNew(): boolean {
        const novelInfo = this.getInfo();
        return novelInfo.update === null;
    }

    protected isUpdated(): boolean {
        const novelInfo = this.getInfo();
        return novelInfo.update !== novelInfo.lastUpdate;
    }

    /**
     * 更新されたコンテンツがあるか
     * ※ 表示するアイコンファイル名の一部を返す
     */
    protected _checkStatus(): string {
        // 小説の更新チェック
        let update = this.isUpdated() ? '_update' : '';

        const novelInfo = this.getInfo();
        
        // ページをチェック
        let pageFile = path.join(nreader.getDataPath(), novelInfo.code, '/page.json');
        
        let unread = false;
        let mod = false;

        if (fs.existsSync(pageFile)) {
            for (let pageItem of this._pageList) {

                if (pageItem.isNew()) {
                    return update + '_page_new';

                } else {
                    if (pageItem.isCached()) {
                        if (pageItem.isUpdated()) {
                            mod = true;
                        }
                    } else {
                        unread = true;
                    }
                }
            }

            if (mod) {
                return update + '_page_mod';
            } else {
                return unread ? update + '_page_unread' : update + '';
            }

        } else {
            return update + '_page_new';
        }
    }

    /**
     * ローカルに保存したページ情報を取得
     */
    public importPageState(): void {
        let stateFile = path.join(nreader.getDataPath(), this._info.code, '/page.json');

        if (fs.existsSync(stateFile)) {
            let pageInfo: type.PageInfo;
            let json = JSON.parse(fs.readFileSync(stateFile).toString());

            for (pageInfo of json.pages) {
                this._pageList.push(new PageItem(this, pageInfo));
            }

        } else {
            this._pageList = [];
        }
    }

    /**
     * ページ情報をローカルに保存
     */
    public exportPageState(): void {
        let stateFile: string = path.join(nreader.getDataPath(), this._info.code, '/page.json');

        let pageInfoList: type.PageInfo[] = [];

        for (let pageItem of this._pageList) {
            pageInfoList.push(pageItem.getInfo());
        }

        fs.writeFileSync(
            stateFile,
            JSON.stringify(
                {
                    pages: pageInfoList
                }
            )
        );
    }

    /**
     * 保持している情報を返す
     */
    public getInfo(): type.NovelInfo {
        return this._info;
    }

    public selectNovel(): void {
        const novelInfo = this.getInfo();
        const pageCache = path.join(nreader.getDataPath(), novelInfo.code, 'page.json');

        /**
         * キャッシュがない、更新日を持たない(=新規)、更新されているか
         * novelInfo.updateFlg : 更新あり
         * novelInfo.update === '' : キャッシュ取得日
         */
        //if (!fs.existsSync(pageCache) || novelInfo.update === "" || novelInfo.update !== novelInfo.lastUpdate) {
        if (!fs.existsSync(pageCache) || this.isUpdated()) {
            this.importNovelPages()
            .then(() => {
                this.showPageList();
            });
        } else {
            this.showPageList();
        }
        
    }

    /**
     * ページ一覧を取得し、各ページの情報を取得、小説の状態を更新・保存する
     * @param {*} ncode 
     */
    public importNovelPages(): Promise<any> {
        const self = this;

        let novelInfo = this.getInfo();
        let ua = nreader.getConfig('UserAgent');

        return axios.default({ method: 'GET', url: novelInfo.url, responseType: 'document', headers: {"User-Agent": ua} })
            .then(function (response: axios.AxiosResponse) {
                if (response.status === 200) {
                    // 一覧から各ページの情報を取得する
                    self._parseNovelPage(response.data);

                    // 更新日設定
                    novelInfo.update = novelInfo.lastUpdate;

                    nreader.refreshProviders();
                    novels.save();
                } else {
                    throw new Error(response.statusText);
                }
            });
    }

    /**
     * 小説情報の更新
     */
    public async checkUpdate() {
        const self = this;

        let novelInfo = this.getInfo();

        this.loading = true;
        nreader.refreshProviders();
        
        // 新しく情報を取得する
        let targetInfo = await nreader.getNovelInfo(novelInfo.code);
        
        if (targetInfo === undefined) {
            this.loading = false;
            nreader.refreshProviders();
            return;
        }

        // 最終更新日が変化していれば、更新フラグを立てる
       // if (novelInfo.lastUpdate !== targetInfo.lastUpdate) {
       //     targetInfo.updateFlg = true;
       // }
        
       novelInfo.author = targetInfo.author;
       novelInfo.code = targetInfo.code;
       novelInfo.ex = targetInfo.ex;
       novelInfo.lastUpdate = targetInfo.lastUpdate;
       novelInfo.title = targetInfo.title;
       novelInfo.url = targetInfo.url;

        // 少なくとも100ミリ秒更新アイコンを表示させる
        await new Promise((resolve) => {
            setTimeout(() => {
                self.loading = false;
                nreader.refreshProviders();

                resolve();
            }, 100);
        });
    }

    /**
     * HTMLの解析
     * @param {dom} html 
     */
    protected _parseNovelPage(html: string): void {

        var existsUpdate = false;

        try {
            const $: cheerio.Root = cheerio.load(html);

            if ($('#novel_honbun').length > 0) {
                // 単ページ
                existsUpdate = this._parseSinglePage($);

            } else {
                // 複数ページ
                existsUpdate = this._parseMultiPage($);
            }

            // 更新されたものが未読の場合、フラグを落とす
            //if (!existsUpdate) {
            //    this.getInfo().updateFlg = false;
            //}

            // ページの状態保存
            this.exportPageState();

        } catch (e) {
            console.error(e);
        }
    }

    /**
     * 単ページのParse
     * @param {CheerioStatis} $ Cheerio
     */
    protected _parseSinglePage($: cheerio.Root): boolean {
        const novelInfo = this.getInfo();

        let pageInfo: type.PageInfo;

        if (this._pageList.length === 0) {
            pageInfo = {
                title: novelInfo.title,
                code: novelInfo.code,
                url: `/${novelInfo.code}/`,
                lastUpdate: '',
                update: null,
                cached: false,
                no: null
            };
        } else {
            pageInfo = this._pageList[0].getInfo();
            pageInfo.title = novelInfo.title;
        }

        // 最終更新日に変化があれば、更新フラグを立てる
        //if (pageInfo.update !== pageInfo.lastUpdate) {
        //    pageInfo.updateFlg = true;
        //}

        pageInfo.lastUpdate = novelInfo.lastUpdate;

        this.clearPage();
        this.addPage(new PageItem(this, pageInfo));

        return true;
    }

    /**
     * コンテンツが複数あるページのParse
     * @param {CheerioStatic} $ Cheerioでparseされたオブジェクト
     */
    protected _parseMultiPage($: cheerio.Root): boolean {
        const novelInfo = this.getInfo();

        let keepPages = this._pageList.slice();
        let pageIndex: { [index: string]: number } = {};
        let existsUpdate = false;

        // URLがページ固有のキー替わり
        for (let i in this._pageList) {
            pageIndex[this._pageList[i].getInfo().url] = parseInt(i);
        }

        this.clearPage();

        var pageInfo: type.PageInfo;

        $('div.index_box').children().each((i: number, elem: any) => {
            let cs: cheerio.Cheerio = $(elem);

            if (cs.hasClass('chapter_title')) {

                pageInfo = {
                    title: cs.text(),
                    code: novelInfo.code,
                    url: '',
                    lastUpdate: '',
                    update: null,
                    cached: false,
                    no: null
                };

                this.addPage(new PageItem(this, pageInfo));
            }

            if (cs.hasClass('novel_sublist2')) {

                // 最終更新日
                let updateHtml = $('dt.long_update', elem).html();

                if (updateHtml !== null) {
                    let updates = updateHtml.match(/(\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2})/g);
                    let url = $('a', elem).attr('href');

                    if (url === undefined) { url = ''; }
                    let wkNo = url.match(/(^.+)\/(.+)\/$/);
                    let no = wkNo === null ? null : parseInt(wkNo[2]);

                    if (updates !== null) {

                        if (url in pageIndex) {
                            pageInfo = keepPages[pageIndex[url]].getInfo();

                        } else {
                            pageInfo = {
                                title: $('a', elem).text(),
                                code: novelInfo.code,
                                url: url,
                                lastUpdate: '',
                                update: null,
                                cached: false,
                                no: no
                            };
                        }

                        //if (pageInfo.lastUpdate !== updates[updates.length - 1]) {
                        //    pageInfo.updateFlg = true;
                        //}

                        pageInfo.lastUpdate = updates[updates.length - 1];

                        this.addPage(new PageItem(this, pageInfo));

                        if (pageInfo.update !== "" && pageInfo.update !== pageInfo.lastUpdate) {
                            existsUpdate = true;
                        }
                    }
                }
            }

        });

        return existsUpdate;
    }

    /**
     * 検索結果の小説の情報をTreeItemのリストで返す
     */
    public getSearchNovelItemList(): vscode.TreeItem[] {
        return [
            new vscode.TreeItem('Ｎコード：' + this._info.code, vscode.TreeItemCollapsibleState.None),
            new vscode.TreeItem('作者：' + this._info.author, vscode.TreeItemCollapsibleState.None),
            new vscode.TreeItem('URL：' + this._info.url, vscode.TreeItemCollapsibleState.None),
            new vscode.TreeItem('最終更新日：' + this._info.lastUpdate, vscode.TreeItemCollapsibleState.None),
            new vscode.TreeItem('内容：' + this._info.ex, vscode.TreeItemCollapsibleState.None)
        ];
    }

    /**
     * ページのリストを取得
     */
    public getPageItemList(): PageItem[] {
        return this._pageList;
    }

    /**
     * ItemPageを追加
     * @param {*} pageInfo 
     */
    public addPage(pageItem: PageItem): void {
        this._pageList.push(pageItem);
    }

    /**
     * ページリストのクリア
     */
    public clearPage(): void {
        this._pageList.length = 0;
    }

    /**
     * この小説の情報を表示する
     */
    public showInfoPage() {
		let infoPanel = new panel.InfoPanel(this);
		infoPanel.show();
    }

    /**
     * ページ一覧
     * WebViewでページ一覧を表示する
     * ※未使用
     * ※読んだあと、TreeViewと違い更新が必要(messageを使う？)
     * 
     */
    public showPageList(): void {
		if (this._pageListPanel === null) {
			this._pageListPanel = new panel.PageListPanel(this);
		}

		this._pageListPanel.show();
    }

	public updatePageList() {
		if (this._pageListPanel === null) {
			return;
		}

		try {
			this._pageListPanel.update();
		} catch(e) {
		}
	}

}
