import * as vscode from 'vscode';
import * as cheerio from 'cheerio';
import * as axios from 'axios';

import * as nreader from './nreader';
import * as type from './type';


import * as panel from './panel';

const SEARCH_URL = "https://yomou.syosetu.com/search.php?search_type=novel&word={word}&button=";

let isSearch: boolean = false;

/**
 * 検索処理
 * @param {String} word 検索ワード
 */
export function search(word: string): void {
    if (isSearch) {
        vscode.window.showInformationMessage('検索処理中です');
        return;
    }

    let url = SEARCH_URL.replace('{word}', encodeURI(word));
    let ua = nreader.getConfig('UserAgent');

    isSearch = true;

    axios.default({
        method: 'GET', url: url, responseType: 'document',
        params: {
            search_type: 'novel',
            word: word,
            button: ''
        },
        headers: {"User-Agent": ua}
    })
        .then(function (response: { [index: string]: any }) {
            // 検索結果ページを解析
            if (response.status === 200) {
                parseResult(word, response.data);
            }
        })
        .catch((e: any) => {
            vscode.window.showErrorMessage(e.message);
        })
        .finally(() => {
            isSearch = false;
        });
}

/**
 * 検索結果のHTMLをWebView用に構成し直し
 * @param {String} html 
 */
function parseResult(word: string, html: string): void {
    let body = '';

    try {
        const $ = cheerio.load(html);
        
        $('div.searchkekka_box').each((i: number, elem: any) => {
            let novelInfo: type.NovelInfo = parseNovelElement($, elem);

            body += `
                <h2 class=title>${novelInfo.title}</h2>\n
                <span class=auth>作者：${novelInfo.author}</span>
                <span class=ncode>Ｎコード：${novelInfo.code}</span>
                <span class=lastUpdate>最終更新日：${novelInfo.lastUpdate}</span>
                <span class="add" onClick="postMessage( { command: 'addNovel', ncode: '${novelInfo.code}' } );return false;">[本棚に追加]</span>
                <br><br>
                ${novelInfo.ex}<br><br>
            `;
        });

		const resultPanel = new panel.BasePanel();

		resultPanel.title = '検索：' + word;
		resultPanel.content = body;
		resultPanel.recieveMessage = recieveMessage;

		resultPanel.show();
        
    } catch (e) {
        vscode.window.showErrorMessage(e.message);
    }
}

/**
 * 小説情報のエレメントをNovelInfoに変換する
 * @param {cheerio}} $ cheerioオブジェクト
 * @param {*} elem 該当エレメント
 */
function parseNovelElement($: any, elem: any): type.NovelInfo {

    let titleElem = $('div.novel_h', elem);
    let url = $('a', titleElem).attr('href');
    let result = url.match(new RegExp("https:\/\/ncode\.syosetu\.com\/(.+)\/"));

    let lastUpdate = $('table', elem).text().match(/最終更新日：(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2})/);

    return {
        title: titleElem.text(),
        url: url,
        code: result[1].toLowerCase(),
        author: $('a[href^="https://mypage.syosetu.com/"]', elem).text(),
        ex: $('div.ex', elem).text().replace(/\n/g, '<br>'),
        lastUpdate: lastUpdate[1],
        update: null
    };
}

/**
 * 検索結果のWebViewからのメッセージを受け取る
 * ・addNovel - 受け取ったNコードの情報を取得し、addNovelコマンド処理を実行
 * @param ncode Nコード
 */
function recieveMessage(message: any) : void{
    
    switch (message.command) {
        case 'addNovel':
            nreader.command.addNovel(message.ncode);

            break;

        default:
            break;
    }
}