import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as axios from 'axios';
import * as cheerio from 'cheerio';

import * as nreader from '../nreader';
import * as novels from '../novels';
import * as panel from '../panel';
import * as type from "../type/pageInfo";
import { BaseItem } from './baseItem';
import { NovelItem } from './novelItem';
import { PageState } from '../pageState';
import { INrItem } from '../interface/nrItem';

/**
 * ページのTreeItem
 * ※ 今はページをTreeView表示していない
 */
export class PageItem extends BaseItem implements INrItem {
    protected _novelItem: NovelItem;
    protected _info: type.PageInfo;
    public loading: boolean = false;

    constructor(novelItem: NovelItem, info: type.PageInfo) {
        super(info, vscode.TreeItemCollapsibleState.None);

        this._novelItem = novelItem;
        this._info = info;
        this.contextValue = 'Page';

        // TreeItemクリックされた際のコマンド
        this.command = {
            command: "novelReader.selectNovelPage",
            title: "Select Node",
            arguments: [this]
        };
    }

    /**
     * アイコンのパスを返す
     */
    public get iconPath(): string {
        return '';
    }

    /**
     * 未読、既読
     * ※ キャッシュがあれば、既読
     */
    public isCached() {
        const pageInfo = this.getInfo();
        return pageInfo.cached;
    }

    /**
     * 新規・既読更新
     */
    public isUpdated() {
        const pageInfo = this.getInfo();
        return pageInfo.update !== pageInfo.lastUpdate;
    }

    /**
     * 新規かどうか
     * キャッシュなし、更新なしの場合、新規とする
     */
    public isNew() {
        const pageInfo = this.getInfo();
        return (!this.isCached() && pageInfo.url !== '' && pageInfo.update === null);
    }

    /**
     * 基本情報取得
     */
    public getInfo(): type.PageInfo {
        return this._info;
    }

    /**
     * ページ選択
     * このPageItemが管理しているページを必要であれば取得し、WebViewで表示させる
     */
    public selectPage(): void {
        const self = this;
        const info = this.getInfo();
		
		const pagePanel = new panel.PagePanel(this);
		pagePanel.show();
        
        //nreader.refreshProviders();
    }

    /**
     * 親のNovelItemを取得
     */
    protected _getNovel(): NovelItem {
        return novels.get(this._info.code);
    }
	
	/**
     * ページキャッシュファイルの取得
     */
    public getPageFile(): string {
        let info = this.getInfo();
        let no: string = info.no === null ? '0' : info.no.toString();

        return path.join(nreader.getDataPath(), info.code, no);
	}
    /**
     * ページコンテンツを取得 
     */
    public getPageContent() : Promise<string> {
        const self = this;
        const pageInfo = this.getInfo();
        const pageFile = this.getPageFile();

        return new Promise((resolve, reject) => {

            // キャッシュがあり最新状態ならキャッシュを表示させる
            if (fs.existsSync(pageFile) && pageInfo.update === pageInfo.lastUpdate) {
                resolve(fs.readFileSync(pageFile, 'utf8'));
                return;
            }

            // キャッシュをWEBから取得し、ローカルに保存
            let ua = nreader.getConfig('UserAgent');

            axios.default({ method: 'GET', url: `https://ncode.syosetu.com${pageInfo.url}`, responseType: 'document', headers: {"User-Agent": ua} })
                .then(function (response: axios.AxiosResponse) {

                    if (response.status === 200) {
                        // ページキャッシュ更新と更新日設定
                        self._updatePage(response.data);

                        const novelItem = self._getNovel();

                        // ページ情報を出力
                        novelItem.exportPageState();

                        novels.save();

                        resolve(fs.readFileSync(self.getPageFile(), 'utf8'));
                    }
                })
                .catch(reason => {
                    reject(`ページの取得に失敗しました：${reason}`);
                });
        });
	}
	
    /**
     * HTMLの本文を抜き出し
     * @param {String}} html HTMLソース
     */
    protected _updatePage(html: string): void {

        const pageFile = this.getPageFile();

        fs.mkdir(path.dirname(pageFile), { recursive: true }, (err: any) => {
            vscode.window.showErrorMessage(err);
            return;
        });

        try {
            /*
             * キャッシュの保存(Markdown形式)
             * ・本文部分のHTMLを保存
             * → 表示時にHTMLヘッダー等をつけてWebView表示させる
             */
            const $ = cheerio.load(html, { decodeEntities: false });

            // 本文をキャッシュとして保存
            fs.writeFileSync(pageFile, $('div#novel_honbun').html());

            // 更新日に最終更新日を設定
            let pageInfo = this.getInfo();
            
            pageInfo.update = pageInfo.lastUpdate;
            pageInfo.cached = true;

        } catch (e) {
            vscode.window.showErrorMessage(e.message);
        }
	}
	
	/**
	 * ページの状態を取得する
	 */
	public getPageState() : PageState {
		if (this.isNew()) {
			return PageState.New;
		} else {
			if (this.isCached()) {
				if (this.isUpdated()) {
					return PageState.Mod;
				}
			} else {
				return PageState.Unread;
			}
		}

		return PageState.Read;
	}
}
