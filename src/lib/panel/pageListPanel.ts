import * as vscode from 'vscode';
import * as nreader from '../nreader';
import * as treeItem from '../treeItem';
import * as type from "../type/pageInfo";

import { ItemPanel } from './itemPanel';
import { PagePanel } from './pagePanel';
import { PageState } from '../pageState';
import { PageItem } from '../treeItem';

/**
 * ページリストパネル
 */
export class PageListPanel extends ItemPanel {
	
	public constructor(novelItem: treeItem.NovelItem) {
		super(novelItem);
	}

	/**
	 * 保持しているNovelItelのプロパティ
	 */
	protected get novelItem(): treeItem.NovelItem {
		return this.item as treeItem.NovelItem;
	}

	/**
	 * ページ一覧からの操作を受ける
	 */
	protected recieveMessageCallback(message: any): void {
		switch (message.command) {
			case 'selectPage':
				const pageItem = this.novelItem.getPageItemList()[parseInt(message.no)];
				const pagePanel = new PagePanel(pageItem);

				pagePanel.show();

				return;
				
			case 'novelInfo':
				nreader.command.showNovelInfo(this.novelItem);
				return;

			case 'removeNovel':
				nreader.command.removeNovel(this.novelItem);
				return;

			default:
				throw new Error('コマンドエラー：' + message.command);
		}
	}

    /**
     * WebViewの本体部分の作成
     */
    public get content(): string {
		const novelItem = this.novelItem;
		const novelInfo = novelItem.getInfo();
		
        let pageItem: treeItem.PageItem;
        let pageInfo: type.PageInfo;

        let body: string = '';
        let state: string;

        body +=  `
        	<h2 class=title>${novelInfo.title}</h2>\n
        	<span class=auth>作者：${novelInfo.author}</span>
        	<span class=ncode>Ｎコード：${novelInfo.code}</span>
			<span class=lastUpdate>最終更新日：${novelInfo.lastUpdate}</span>
			<span class=enable onClick="postMessage( {command: 'novelInfo', ncode: '${novelInfo.code}' } );return false;">[情報]</span>
			<span class=enable onClick="postMessage( {command: 'removeNovel', ncode: '${novelInfo.code}' } );return false;">[削除]</span>
			<br><br>
    	`;

		const pageList = novelItem.getPageItemList();

        for (let i in pageList) {
            pageItem = pageList[i];
			pageInfo = pageItem.getInfo();

            if (pageInfo.url === '') {
				// チャプター
                body += `<span class=chapter>${pageInfo.title}</span>`;
            } else {
				
				state = pageItem.getPageState();

				if (state in PageState) {
					state += 'Page';
				} else {
					throw new Error('予期しないページ状態が返されました：' + pageItem.getPageState());
				}

                body += `<span class="select ${state}" onClick="postMessage({command: 'selectPage' ,no: ${i}});return false;">・${pageInfo.title}</span>`;
                body += `　最終更新日：${pageInfo.lastUpdate}`;
			}

            body += `<br>\n`;
        }

        return body;
    }
}
