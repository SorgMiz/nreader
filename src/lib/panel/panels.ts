import { BasePanel } from "./basePanel";
import { BaseItem } from "../treeItem";
import { IPanel } from "../interface/panel";
import { IItemPanel } from "../interface/itemPanel";
import { INrItem } from "../interface/nrItem";

/**
 * パネル
 * panelオブジェクトが作成されたら、Panels.addするようにする
 */
class Panels {
	private static panelList: IPanel[] = [];

	private constructor() {}

	public static addPanel(basePanel: BasePanel) {
		Panels.panelList.push(basePanel);
	}

	/**
	 * Panelで保持
	 * @param baseItem 
	 */
	public static update(nrItem: INrItem) {
		for(let panel of Panels.panelList) {
			let itemPanel = (panel as IItemPanel);

			if (itemPanel.updateItem !== undefined) {
				itemPanel.updateItem(nrItem);
			}
		}
	}

	public static existsPanel(): boolean {
		return false;
	}
}