import * as vscode from 'vscode';
import * as type from "../type";

export abstract class BaseItem extends vscode.TreeItem {
    protected _info: type.BaseInfo;

    constructor(info: type.BaseInfo, CollapsibleState: vscode.TreeItemCollapsibleState) {
        super(info.title, CollapsibleState);
        this._info = info;
    }

    public abstract get iconPath(): string;
    public abstract getInfo(): type.BaseInfo;
}
